#!/usr/bin/env bash
# Generates gource video (h.264) out of multiple repositories.
# Pass the repositories in command line arguments.
# Example:
# <this.sh> /path/to/repo1 /path/to/repo2
# Or use ls -d * to list all folders in a location
# <this.sh> `ls -d *`

RESOLUTION="1920x1080"  # HD
# RESOLUTION="2560x1440"  # QHD
# RESOLUTION="4096x2160"  # 4K
TITLE="Software Development"
# Title to snake_case with date and file extension
OUTPUT=`sed -e 's/\(.*\)/\L\1/' <<< "$TITLE"| sed -r 's/[ ]+/_/g'`_`date +%F_%H-%M
`.mp4
BG_COLOUR="38383D"
FPS="60"

install_deps() {
    # List of dependencies
    deps=(gource ffmpeg)
    pkgs=""
    # Check which pkgs are not installed and make a list
    for pkg in ${deps[*]}
    do
        PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $pkg|grep "install ok installed")
        echo Checking for $pkg: $PKG_OK
        if [ "" = "$PKG_OK" ]; then
          echo "No $pkg."
          pkgs="$pkgs $pkg"
        fi
    done
    # Install missing packages
    if [ -n "$pkgs" ]; then
        sudo apt install $pkgs
    fi
}

make_video() {
    i=0
    tmp_log=/tmp/gource.XXXXXX
    for repo in $*; do
        # 1. Generate a Gource custom log files for each repo.
        # This can be facilitated by the --output-custom-log FILE
        logfile="$(mktemp $tmp_log)"
        gource --output-custom-log "${logfile}" ${repo}
        # 2. If you want each repo to appear on a separate branch instead of merged
        # onto each other (which might also look interesting), you can use a 'sed'
        # regular expression to add an extra parent directory to the path of the
        # files in each project:
        sed -i -E "s#(.+)\|#\1|/${repo}#" ${logfile}
        logs[$i]=$logfile
        let i=$i+1
    done

    combined_log="$(mktemp $tmp_log)"
    cat ${logs[@]} | sort -n > $combined_log
    rm ${logs[@]}

    echo ""
    echo "Committers:"
    cat $combined_log | awk -F\| {'print  $2'} | sort | uniq
    echo "======================"

    time gource $combined_log \
        -s 0.55 \
        -i 0 \
        -$RESOLUTION \
        --screen 2 \
        --highlight-users \
        --highlight-dirs \
        --file-extensions \
        --hide mouse,filenames \
        --background-colour $BG_COLOUR \
        --auto-skip-seconds .1 \
        --key \
        --stop-at-end \
        --camera-mode overview \
        --title "$TITLE" \
        --user-image-dir users \
        --output-framerate $FPS \
        --frameless \
        --output-ppm-stream - \
        | ffmpeg -y \
        -r $FPS \
        -f image2pipe \
        -vcodec ppm \
        -i - \
        -vcodec libx264 \
        -preset fast \
        -crf 18 \
        -threads 0 \
        -bf 0 \
        -pix_fmt yuv420p \
        -movflags \
        +faststart \
        $OUTPUT
    rm $combined_log
}

install_deps &
make_video $*
