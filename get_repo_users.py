"""Get all commiters form a repo and ftech their gravatar"""

import argparse
import hashlib
import requests
import git

def fetch_gravatar(email):
    # hash the email address using MD5
    hash = hashlib.md5(email.encode('utf-8')).hexdigest()

    # construct the Gravatar URL
    url = f'https://www.gravatar.com/avatar/{hash}'

    # make a GET request to the Gravatar URL
    response = requests.get(url)

    # check if the request was successful
    if response.status_code == 200:
        # return the image data
        return response.content
    else:
        # return None if the request failed
        return None


def get_user_list(repo):
    # get all the submodules
    submodules = repo.submodules

    # get the commiters in the main repository
    commiters = set([(commit.author.email, commit.author.name) for commit in repo.iter_commits()])
    # print(f'Commiters in main repository: {commiters}')

    # iterate over all the submodules and get their commiters
    for submodule in submodules:
        submodule_repo = submodule.module()
        submodule_commiters = set((commit.author.email, commit.author.name) for commit in submodule_repo.iter_commits())
        commiters = commiters.union(submodule_commiters)

    commiter_list = list(commiters)
    commiter_list.sort()
    return commiter_list

def main():

    # create a command-line argument parser
    parser = argparse.ArgumentParser(description='List commiters in a Git repository and its submodules')
    parser.add_argument('path', type=str, help='Path to the Git repository')
    parser.add_argument('users', type=str, help='Path to the Git repository')

    # parse the command-line arguments
    args = parser.parse_args()

    # initialize the Git repo object
    repo = git.Repo(args.path)

    consolidated_users = dict()

    for (email, name) in get_user_list(repo):
        if email not in consolidated_users:
            consolidated_users[email] = [name]
        else:
            consolidated_users[email].append(name)
        image_data = fetch_gravatar(email)
        if image_data is not None:
            with open(f'{args.users}/{name}.png', 'wb') as f:
                f.write(image_data)
        else:
            print('Failed to fetch Gravatar.')

    for email, names in consolidated_users.items():
        print(f"{email}: {names}")

if __name__ == '__main__':
    main()
